#[derive(Clone, Copy)]
pub struct Day(u8);

#[derive(Clone, Copy)]
pub struct Month(u8);

#[derive(Clone, Copy)]
pub struct Year(u16);

#[derive(Clone, Copy)]
pub struct Date {
    year: Option<Year>,
    month: Option<Month>,
    day: Option<Day>,
}

impl Date {
    fn undefined() -> Date {
        Date {
            year: None,
            month: None,
            day: None,
        }
    }
}

#[derive(Clone, Copy)]
pub struct VolumeIndex {
    value: Option<i32>,
    count: Option<i32>,
}

impl VolumeIndex {
    fn undefined() -> VolumeIndex {
        VolumeIndex {
            value: None,
            count: None,
        }
    }
}

#[derive(Clone)]
pub struct IssueIndex {
    value: Option<String>,
    count: Option<String>,
}

impl IssueIndex {
    fn undefined() -> IssueIndex {
        IssueIndex {
            value: None,
            count: None,
        }
    }
}

#[derive(Clone)]
pub struct Series(String);

#[derive(Clone)]
pub struct Title(String);

#[derive(Clone)]
pub struct Publisher(String);

pub struct Metadata {
    series: Option<Series>,
    title: Option<Title>,
    issue: IssueIndex,
    volume: VolumeIndex,
    publisher: Option<Publisher>,
    date: Date,
}

impl Metadata {
    fn empty() -> Metadata {
        Metadata {
            series: None,
            title: None,
            issue: IssueIndex::undefined(),
            volume: VolumeIndex::undefined(),
            publisher: None,
            date: Date::undefined(),
        }
    }
}

type Transformer = Box<dyn Fn(Metadata) -> Metadata>;

fn set_title(value: Title) -> Transformer {
    return Box::new(move |m: Metadata| Metadata {
        title: Some(value.clone()),
        ..m
    })
}

fn set_series(value: Series) -> Transformer {
    return Box::new(move |m: Metadata| Metadata {
        series: Some(value.clone()),
        ..m
    })
}

fn set_date(value: Date) -> Transformer {
    return Box::new(move |m: Metadata| Metadata {
        date: value,
        ..m
    })
}
