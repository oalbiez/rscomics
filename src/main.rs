pub mod comics;

use comics::utils::compose::compose_two;
use rand::Rng;
use std::cmp::Ordering;
use std::io;
use std::string::String;

fn main() {
    let add = |x: i32| x + 2;
    let multiply = |x: i32| x * 2;
    let divide = |x: i32| x / 2;
    let count = |x: String| x.chars().count();
    let to_i32 = |x: usize| x as i32;

    let intermediate = compose!(count, to_i32, add, multiply, divide);

    println!("paf: {}", intermediate(String::from("AB")));

    println!("Devinez le nombre !");

    let nombre_secret = rand::thread_rng().gen_range(1..101);

    loop {
        println!("Veuillez entrer un nombre.");

        let mut supposition = String::new();

        io::stdin()
            .read_line(&mut supposition)
            .expect("Échec de la lecture de l'entrée utilisateur");

        let supposition: u32 = match supposition.trim().parse() {
            Ok(nombre) => nombre,
            Err(_) => continue,
        };

        println!("Votre nombre : {}", supposition);

        match supposition.cmp(&nombre_secret) {
            Ordering::Less => println!("C'est plus !"),
            Ordering::Greater => println!("C'est moins !"),
            Ordering::Equal => {
                println!("Vous avez gagné !");
                break;
            }
        }
    }
}
